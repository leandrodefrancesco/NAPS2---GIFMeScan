using System;
using System.Collections.Generic;
using System.Linq;
using NAPS2.DI.EntryPoints;

namespace NAPS2
{
    static class Program
    {
        /// <summary>
        /// The NAPS2.exe main method.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Singleton.ParametrosInicio.Instance.SetId(args[0]);
            Singleton.ParametrosInicio.Instance.SetTipoOperacion(args[1]);
            Singleton.ParametrosInicio.Instance.SetNombre(args[2]);
            typeof(WinFormsEntryPoint).GetMethod("Run").Invoke(null, new object[] { args });
        }
    }
}
