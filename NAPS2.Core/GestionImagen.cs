﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Conexion;

namespace GestionImagen{
    class GestionImagen{
        public static void cargarImagenABD(Byte[] imagen, string id, string tipoOperacion, string nombreImagen){
            SqlConnection cnn = Conexion.Conexion.conectarBD("GIFMeI");
            string sql = "GIFMeC.dbo.General_Gestionar_Escaner";
            try{
                using (SqlCommand cmd = new SqlCommand(sql, cnn)){
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@imagen", SqlDbType.VarBinary).Value = imagen;
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
                    cmd.Parameters.Add("@tipoOperacion", SqlDbType.VarChar).Value = tipoOperacion;
                    cmd.Parameters.Add("@nombreImagen", SqlDbType.VarChar).Value = nombreImagen;
                    cnn.Open();
                    if (cnn.State == ConnectionState.Open){
                        Int32 rowsAffected = cmd.ExecuteNonQuery();
                        cnn.Close();
                        MessageBox.Show("¡Imagen cargada con éxito!");
                    }
                }
            }catch (Exception ex){
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

