﻿using System;
namespace Singleton{
    public sealed class ParametrosInicio
    {
        private readonly static ParametrosInicio _instance = new ParametrosInicio();
        private string Id;
        private string TipoOperacion;
        private string Nombre;

        private ParametrosInicio(){
        }

        public static ParametrosInicio Instance
        {
            get{
                return _instance;
            }
        }

        public string SetId(string id)
        {
            this.Id = id;
            return this.Id;
        }

        public string SetTipoOperacion(string tipoOperacion)
        {
            this.TipoOperacion = tipoOperacion;
            return this.TipoOperacion;
        }

        public string GetTipoOperacion()
        {
            return this.TipoOperacion;
        }
        public string GetId()
        {
            return this.Id;
        }

        public string GetNombre()
        {
            return this.Nombre;
        }
        public string SetNombre(string nombre)
        {
            this.Nombre= nombre;
            return this.Nombre;
        }
    }
}
