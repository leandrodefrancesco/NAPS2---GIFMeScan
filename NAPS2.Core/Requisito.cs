﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Requisito
{
    public class Requisito{
        private string nombre;

        public Requisito()
        {
        }

        public string GetNombre(){
            return this.nombre;       
        }
        public void SetNombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string obtenerRequisitoDeBD(string Id){
            SqlDataReader nombreRequisito;
            SqlConnection conexion = Conexion.Conexion.conectarBD("GIFMeC");
            string sql = "SELECT Descripcion FROM GIFMeC.dbo.Requisitos WHERE Id = @id";
            try{
                using (SqlCommand cmd = new SqlCommand(sql, conexion))
                {
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = Id;
                    try{
                        conexion.Open();
                        if (conexion.State == ConnectionState.Open){
                            nombreRequisito = cmd.ExecuteReader();                
                            while (nombreRequisito.Read()){
                                this.SetNombre(nombreRequisito[0].ToString());
                            }
                            conexion.Close();
                        }
                    }
                    catch (Exception ex){
                        MessageBox.Show("Excepción es: " + ex.Message);
                    }
                }
            }
            catch (SqlException ex){
                MessageBox.Show(ex.Message);
            }
            return this.nombre;
        }
    }
}
